const productData = [
	{
		id: "001",
		name: "Coke",
		description: "Coke with Regular Amount of Sugar",
		price: 80,
		onOffer: true
	},

	{
		id: "002",
		name: "Sprite",
		description: "Sprite with Regular Amount of Sugar",
		price: 85,
		onOffer: true
	},
	
	{
		id: "003",
		name: "Royal",
		description: "Royal with Regular Amount of Sugar",
		price: 90,
		onOffer: true
	}
]

export default productData;