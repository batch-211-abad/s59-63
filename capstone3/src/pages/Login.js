import {Form, Button} from 'react-bootstrap';
//Complete (3) Hooks of React
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';


export default function Login() {


  const [email, setEmail] = useState ('');
  const [password, setPassword] = useState ('');
  const [isActive, setIsActive] = useState ('');

  console.log(email);
  console.log(password);

  // Allows us to consume the User Context/Data and its properties for validation
  const { user, setUser } = useContext(UserContext);


  //refactor the authenticate function in the Login page and create a fetch request which will allow us to integrate our backend Express JS application to our FE React JS application
  function authenticate(e) {

      e.preventDefault()

      // Process a fetch request to the corresponding backend API
      // The header information "Content-Type" is used to specify that the information being sent to the backend will be sent in the form of JSON
      // The fetch request will communicate with our backend application providing it with a stringified JSON
      // Convert the information retrieved from the backend into a JavaScript object using the ".then(res => res.json())"
      // Capture the converted data and using the ".then(data => {})"
      // Syntax
          // fetch('url', {options})
          // .then(res => res.json())
          // .then(data => {})

      // fetch(`${process.env.REACT_APP_API_URL}/users/login`, {


      fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
        method: 'POST',
        headers:{
          'Content-Type':'application/json'
        },
        body: JSON.stringify({
          email: email,
          password: password
        })
      })
      .then(res=>res.json())
      .then(data=>{

        // It is good practice to always print out the result of our fetch request to ensure that the correct information is received in our frontend application
        console.log(data);

        //Create a conditional statement that will store the generated JWT token in the localStorage to allow us to use it in retrieving the users's information when needed.

        // If no user information is found, the "access" property will not be available and will return undefined
        // Using the typeof operator will return a string of the data type of the variable/expression it preceeds which is why the value being compared is in a string data type

        if(typeof data.access !== "undefined"){

          // The JWT will be used to retrieve user information across the the whole frontend application and storing it in the localStorage will allow ease of access to the user's information

          localStorage.setItem('token',data.access)

          //we invoke this function when logging in

          retrieveUserDetails(data.access)


          Swal.fire({
            title: "Login Successful!",
            icon: "success",
            text: "Welcome to your dashboard"
          });

        }else{

          Swal.fire({
            title: "Authentication Failed!",
            icon: "error",
            text: "Check your credentials!" 
          });
          
        }
      })

      //This function that will convert the JWT retrieved from the fetch request created in the "authenticate" function.

      const retrieveUserDetails = (token) =>{

        // The token will be sent as part of the request's header information
        // We put "Bearer" in front of the token to follow implementation standards for JWTs

      //The environment variables are important for hiding sensitive pieces of information like the backend API URL which can be exploited if added directly into our code.
      //The environment variables of a React JS app is only applied to an application upon initial start up of the application.
      //Make sure to restart the frontend application after creating this file to ensure that it will be used within the project.
        //${process.env.REACT_APP_API_URL}
        //.env.local
        //REACT_APP_API_URL=http://localhost:4000

        // fetch(`${process.env.REACT_APP_API_URL}/users/getUserDetails`, {
        
        fetch(`${process.env.REACT_APP_API_URL}/users/getUserDetails`, {
          headers: {
            Authorization: `Bearer ${token}`
          }
        })
        .then(res=>res.json())
        .then(data=>{
          console.log(data);

          // Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation across the whole application

          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          });
        })
      }

      //set the email of the authenticated user in the local storage
      /*
        Syntax:
            localStorage.setItem("propertyName",value)
      */

      // localStorage.setItem("email",email)
      //the "localStorage.setItem" allows us to manipulate the browser's localStorage property to store information indefinitely to help demonstrate conditional rendering and the login and logout features.
      //Because React JS is a single page application, using the localStorage does not trigger rerendering of components and for us to be able to view the effects of this we would need to refresh our browser.
      //The proper solution to this will be discussed in the next session.

      // Set the global user state to have properties obtained from local storage.
      // This will pass the data to the UserContext which is ready to use from all different endpoints/pages
      // setUser({
      //   email: localStorage.getItem('email')
      // })

      setEmail("");
      setPassword("");

      // alert(`${email} has been verified! Welcome back!`)
  }

  useEffect(()=>{

    if(email !== "" && password !== ""){
      setIsActive(true)
    } else {
      setIsActive(false)
    }
 

  }, [email, password])



  return (
    // Conditional Rendering
    // LOGIC - if there is a user logged-in in the web application, enpoint or "/login" should not be accesible. The user should be navigated to courses tab instead.

    //we use the id of the user to redirect the user back to the courses page when the id is not null
    (user.id !== null)
    ?
    <Navigate to="/admin"/>
    // ELSE - if the localStorage is empty, the user is allowed to access the login page.
    :
    <Form onSubmit={(e)=>authenticate(e)}>
      <Form.Group className="mb-3" controlId="userEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control
        type="email"
        placeholder="Enter your email"
        value={email}
        onChange={e=>setEmail(e.target.value)}
        required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="password">
        <Form.Label>Password</Form.Label>
        <Form.Control
        type="password"
        placeholder="Password"
        value={password}
        onChange={e=>setPassword(e.target.value)}
        required
        />
      </Form.Group>
      
      { isActive ?
      <Button variant="success" type="submit" id="submitBtn">
        Login
      </Button>
      :
      <Button variant="danger" type="submit" id="submitBtn" disabled>
        Login
      </Button>
      }

    </Form>
  );
}
