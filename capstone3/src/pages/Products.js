// import coursesData from '../data/courses';
import { useEffect, useState, useContext } from 'react';
import { Navigate } from 'react-router-dom';
import ProductCard from '../components/ProductCard';
import UserContext from '../UserContext';

export default function Products(){

	// console.log(coursesData)

	//using the map array method, we can loop through our courseData and dynamically render any number of CourseCards depending on how many array elements are present in our data

	//map returns an array, which we can display in the page via the component function's return statement

	//props are a way to pass any valid JS data from parent component to child component.

	//You can pass as many props as you want. Even functions can be passed as props.

	const [products, setProducts] = useState([])

	const {user} = useContext(UserContext);
	console.log(user);

	// fetch(`${process.env.REACT_APP_API_URL}/products/active`)

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/active`)
		.then(res => res.json())
		.then(data =>{

		const productArr = (data.map(product => {
		console.log(product)
		return (
			<ProductCard productProp={product} key={product._id}/>
				)

	}))
			setProducts(productArr)
})
.catch(error => (console.log(error)));

	},[products])







	

	return(
		<>
			<h6>Products</h6>
			{products}
		</>
	)
}
