import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home(){

	const data = {
		title: "Shazada E-commerce Website",
		content: "Login/Register to avail of the ongoing sale",
		destination: "/login",
		label: "Login Now!"
	}

	return(
		<>
			<Banner bannerProp={data}/>
			<Highlights/>
		</>
	)
}